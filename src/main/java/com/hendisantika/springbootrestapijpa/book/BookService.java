package com.hendisantika.springbootrestapijpa.book;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-api-jpa
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-31
 * Time: 07:58
 * To change this template use File | Settings | File Templates.
 */
public interface BookService {

    boolean isExist(Book book);

    Book save(Book book);

    Book findById(int id);

    List<Book> findAll();

    Book update(Book book);

    void delete(int id);

    void deleteAll();
}