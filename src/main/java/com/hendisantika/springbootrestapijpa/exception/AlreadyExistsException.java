package com.hendisantika.springbootrestapijpa.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-api-jpa
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-30
 * Time: 08:18
 * To change this template use File | Settings | File Templates.
 */
public class AlreadyExistsException extends RuntimeException {

    public AlreadyExistsException(final String message) {
        super(message);
    }
}