package com.hendisantika.springbootrestapijpa.address;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-api-jpa
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-30
 * Time: 08:16
 * To change this template use File | Settings | File Templates.
 */
public interface AddressService {

    boolean isExist(Address address);

    Address save(Address address);

    Address findById(int id);

    List<Address> findAll();

    Address update(Address address);

    void delete(int id);

    void deleteAll();
}