package com.hendisantika.springbootrestapijpa.address;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-api-jpa
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-30
 * Time: 08:15
 * To change this template use File | Settings | File Templates.
 */
@RepositoryRestResource
public interface AddressRepository extends JpaRepository<Address, Integer> {
}