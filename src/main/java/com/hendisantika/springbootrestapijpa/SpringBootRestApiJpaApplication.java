package com.hendisantika.springbootrestapijpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRestApiJpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootRestApiJpaApplication.class, args);
    }

}

