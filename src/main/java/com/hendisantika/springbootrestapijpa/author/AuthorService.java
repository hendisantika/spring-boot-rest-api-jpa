package com.hendisantika.springbootrestapijpa.author;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-api-jpa
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-31
 * Time: 07:53
 * To change this template use File | Settings | File Templates.
 */
public interface AuthorService {

    boolean isExist(Author author);

    Author save(Author author);

    Author findById(int id);

    List<Author> findAll();

    Author update(Author author);

    void delete(int id);

    void deleteAll();
}