package com.hendisantika.springbootrestapijpa.publisher;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-api-jpa
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-31
 * Time: 08:02
 * To change this template use File | Settings | File Templates.
 */
public interface PublisherService {

    boolean isExist(Publisher publisher);

    Publisher save(Publisher publisher);

    Publisher findById(int id);

    List<Publisher> findAll();

    Publisher update(Publisher publisher);

    void delete(int id);

    void deleteAll();
}