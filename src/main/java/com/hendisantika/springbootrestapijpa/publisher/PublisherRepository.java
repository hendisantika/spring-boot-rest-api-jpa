package com.hendisantika.springbootrestapijpa.publisher;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-api-jpa
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-31
 * Time: 08:02
 * To change this template use File | Settings | File Templates.
 */
@RepositoryRestResource
public interface PublisherRepository extends JpaRepository<Publisher, Integer> {
}