# Spring Boot REST API JPA

Things to do :

1. Clone this repo : `git clone https://gitlab.com/hendisantika/spring-boot-rest-api-jpa.git`.
2. Go to the folder : `cd spring-boot-rest-api-jpa`.
3. Create `rest_db` in your PostgreSQL Database.
4. Run the app : `mvn clean spring-boot:run`
